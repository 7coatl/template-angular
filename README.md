# template-angular

Docker Template for Angular

## Getting started

This template generates an environment for angular development in docker and takes the program files from the filesystem of the host machine. 

Requierements:

- Docker Desktop

## Create a project in Windows

```
docker build -t angular .\createProject\.
docker run -itd -v ${PWD}:/app --name angularWeb angular
docker exec -it angularWeb ng new projectName --directory=.
docker rm -f angularWeb
docker-compose up
```

## Create a project in Linux/Mac

```
docker build -t angular ./createProject/ .
docker run -itd -v ${PWD}:/app --name angularWeb angular
docker exec -it angularWeb ng new projectName --directory=.
docker rm -f angularWeb
sudo chown -R $USER:$(id -gn $USER) ./*
docker-compose up
```

## Run a command

```
docker-compose exec web <commandToRun>
```

## Examples

```
docker-compose exec web ng generate component xyz # to create a component
```