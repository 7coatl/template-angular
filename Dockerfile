FROM node:lts-alpine
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install -g @angular/cli
RUN npm install
EXPOSE 4200 49153
CMD ["ng", "serve", "--poll", "500"]